#pragma once

#include <vector>
#include <algorithm>

class DataStructureForMedian
{
public:
	void Insert(const float& val)
	{
		m_Data.push_back(val);
	}

	bool CalculateMedian(float& outMedian)
	{
		size_t size = m_Data.size();

		if (size == 0)
		{
			return false;
		}

		std::sort(m_Data.begin(), m_Data.end());
		if (size % 2 == 0)
		{
			outMedian = (m_Data[size / 2 - 1] + m_Data[size / 2]) / 2;
		}
		else
		{
			outMedian = m_Data[size / 2];
		}

		return true;
	}

private:
	std::vector<float> m_Data;
};