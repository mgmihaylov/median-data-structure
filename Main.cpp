#include <iostream>
#include <string>

#include "DataStructureForMedian.h"

int main(int argc, char** argv)
{
	if (argc <= 2 || strcmp(argv[1], "--insert") != 0)
	{
		std::cout << "Invalid usage!" << std::endl;
		std::string exePath = argv[0];
		size_t lastSlash = exePath.find_last_of('/');
		if (lastSlash == std::string::npos)
		{
			lastSlash = exePath.find_last_of('\\');
		}
		exePath = exePath.substr(lastSlash + 1);
		std::cout << "Usage: " << exePath << " --insert <number1> [<number2>]..."  << std::endl;

		return 1;
	}

	DataStructureForMedian container;

	for (auto i = 2; i < argc; ++i)
	{
		container.Insert(std::stof(argv[i]));
	}

	float median;
	if (container.CalculateMedian(median))
	{
		std::cout << "The median is " << median << std::endl;
	}
	else
	{
		std::cout << "The data structure is empty!" << std::endl;
	}

	return 0;
}