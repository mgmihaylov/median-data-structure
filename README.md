# README #

### What is this repository for? ###

* This repo contains an implementation of a data structure which
  supports two operations (inserting a number and calculating the
  median of the currently stored numbers) and a simple command line
  interface via which you can insert elements inside the data structure.
  
### Setup ###

The project uses CMake 2.6+ for build system. To build and run the project:

* Download and install [CMake](https://cmake.org/download/) if you don't already have it.
* Clone this repo.
* In the root of the repo, run `make .` from the command line.

### Usage ###

Once you build the project, you can run it using the following syntax
from the command line:

```
CalculateMedian.exe --insert <number1> [<number2>]...
```

The application also works with floating point numbers.

### Tests ###

The project also has a simple suite of automated tests. To run them,
navigate to the root of the repository and run

```
ctest
```

from the command line.